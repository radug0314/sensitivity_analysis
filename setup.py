from setuptools import setup

setup(
    name='sensitivity_analysis',
    version='1.0.0',
    author='Radu Ghitescu',
    packages=['sensitivity_analysis'],
    package_dir={'sensitivity_analysis': './sensitivity_analysis'},
    install_requires=[
        'numpy>=1.14.3'
    ],
    zip_safe=False
)