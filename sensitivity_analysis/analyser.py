from collections import OrderedDict
from multiprocessing import cpu_count, Pool
import numpy as np

_N_CORES = cpu_count() - 1


class _SAResult:
    """ Storage for Sensitivity Analysis results"""
    def __init__(self):
        self._ts = OrderedDict()

    def add(self, value, df):
        """ Add returned probabilities for a given value 

        Args:
            value (:obj:`number`) 
                Value of the feature the predictions were made on
            df (:obj:`iterable`) 
                Probabilities
        """
        self._ts[value] = df

    def __iter__(self):
        return (v for v in self._ts.items())


class Analyser:
    """ Perform sensitivity analysis on any model in sklearn

    Args:
        model (:obj:`sklearn.Model`) 
            The model object
        df (:obj:`pandas.DataFrame`)
            Result of running DataFrame.describe() on the target dataset.
            The 'std' row should be dropped before running.
        predict (:obj:`function|method`)
            Function for running the prediction on. Example: model.predict
        get_result (:obj:`function|method`)
            Function for filtering the raw results as returned by the model
            upon running predict.
    """
    def __init__(self, model, df, predict, get_result):
        self._model = model
        self._df = df
        self._predict = predict
        self._get_result = get_result

    def run_on_features(self, features, steps=100, multicore=True):
        """ Run sensitivity analysis on a set of features.
        
        Args:
            features (:obj:`iterable`)
                Features to analyse. All values should be strings.
            steps (:obj:`int`, :optional)
                Number of data points between min and max to predict on. Default is 100.
            multicore (:obj:`bool`, :optional)
                Set this to False to run the predictions on a single process.

        Returns:
            Iterable of tuples (feature, results), where results is a _SAResult object
        """
        if multicore:
            args = ((feature, steps) for feature in features)

            with Pool(_N_CORES) as pool:
                results = pool.starmap(self.run_on_feature, args)
        else:
            results = []
            for feature in features:
                results.append(self.run_on_feature(feature, steps))

        return zip(features, results)

    def run_on_feature(self, feature, steps=100):
        """ Run sensitivity analysis on a single feature.

        Args:
            feature (:obj:`str`)
                Feature to analyse.
            steps (:obj:`int`, :optional)
                Number of data points between min and max to predict on. Default is 100.

        Returns:
            _SAResult object.
        """
        result = _SAResult()

        vals = self._get_step_values(feature, steps)
        df = self._df.copy()

        for val in vals:
            df[feature] = val
            r = self._get_result(self._predict(df))
            result.add(val, r)

        return result

    def _get_step_values(self, feature, steps):
        fd = self._df[feature]

        return np.linspace(min(fd), max(fd), num=steps)

