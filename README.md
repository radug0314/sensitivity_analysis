# Sensitivity Analysis For Prediction Models

### Install

~~~~
pip install git+https://radug0314@bitbucket.org/radug0314/sensitivity_analysis.git
~~~~

### How to use (example)

~~~~
from sensitivity_analysis import Analyser

def get_data(d):
	return d[:, 0]

sanalyser = Analyser(model, dataset, model.predict_proba, get_data)

results = sanalyser.run_on_feature('FeatureName', 100)

for feature_val, predictions in results:
	print(feature_val) # Value of feature for current set of predictions
	print(predictions) # Prediction probabilities for each of the rows in the dataset
~~~~

### API

~~~~
sensitivity_analysis.Analyser.__init__
~~~~

Args:

+ **model (:obj:sklearn.Model):** The model object

+ **df (:obj:pandas.DataFrame):** Result of running DataFrame.describe() on the target dataset. The 'std' row should be dropped before running.

+ **predict (:obj:function|method):** Function for running the prediction on. Example: model.predict

+ **get_result (:obj:function|method):** Function for filtering the raw results as returned by the model upon running predict.


~~~~
sensitivity_analysis.Analyser.run_on_feature
~~~~

Args:

+ **feature (:obj:str):** Feature to analyse.

+ **steps (:obj:int, :optional):** Number of data points between min and max to predict on. Default is 100.

Returns:
    sensitivity_analysis.Analyser._SAResult object.


~~~~
sensitivity_analysis.Analyser.run_on_features
~~~~

Args:

+ **features (:obj:iterable):** Features to analyse. All values should be strings.

+ **steps (:obj:int, :optional):** Number of data points between min and max to predict on. Default is 100.

+ **multicore (:obj:bool, :optional):** Set this to False to run the predictions on a single process.

Returns:
    Iterable of tuples (feature, results), where results is a sensitivity_analysis.analyser._SAResult object

